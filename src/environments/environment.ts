// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBVkEY2eKNr_m3bKnO-OkeT9voVWyYtfwo",
    authDomain: "pocket-moodle-2wiam.firebaseapp.com",
    databaseURL: "https://pocket-moodle-2wiam.firebaseio.com",
    projectId: "pocket-moodle-2wiam",
    storageBucket: "pocket-moodle-2wiam.appspot.com",
    messagingSenderId: "857281461271",
    appId: "1:857281461271:web:6994e92deabb38d3b0f052"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
