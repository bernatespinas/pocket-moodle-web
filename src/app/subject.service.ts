import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore'
import { Observable } from 'rxjs';
import { Subject } from './subject';

@Injectable({
  providedIn: 'root'
})
export class SubjectService {
  
  constructor(
    private firestore: AngularFirestore
  ) { }

  getSubjects(): Observable<Map<string, Subject>> {    
    return new Observable<Map<string, Subject>>((subscriber) => {
      this.firestore.collection('subjects').get().subscribe(snapshot => {
          let subjects: Map<string, Subject> = new Map();
          
          snapshot.forEach(childSnapshot => {
            let subject = new Subject(childSnapshot);

            subjects.set(
              subject.id,
              subject
            );
          });

          subscriber.next(subjects);
        })
      ;
    });
  }

  getSubjectById(id: string): Observable<Subject> {
    return new Observable((subscriber) => {
      this.firestore.collection('subjects').doc(id).get()
        .subscribe((snapshot) => {
          let subject = new Subject(snapshot);

          subscriber.next(subject);
        })
      ;
    });
  }
}
