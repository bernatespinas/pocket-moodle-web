import { DocumentData, QueryDocumentSnapshot } from '@angular/fire/firestore';

export class Subject {
    id: string;

    name: string;
    color: string;

    constructor(snapshot: QueryDocumentSnapshot<DocumentData>) {
        this.id = snapshot.id;

        this.name = snapshot.data().name;
        this.color = snapshot.data().color;
    }
}