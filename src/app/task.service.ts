import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Task } from './topic';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(
    private firestore: AngularFirestore
  ) { }

  getTask(subjectId: string, topicId: string, taskId: string): Observable<Task> {
    return this.getTaskItem(subjectId, topicId, taskId, 'tasks');
  }

  getExam(subjectId: string, topicId: string, examId: string): Observable<Task> {
    return this.getTaskItem(subjectId, topicId, examId, 'exams');
  }

  private getTaskItem(subjectId: string, topicId: string, taskItemId: string, node: string): Observable<Task> {
    return new Observable((subscriber) => {
      this.firestore
        .collection('subjects').doc(subjectId)
        .collection('topics').doc(topicId)
        .collection(node).doc(taskItemId)
      .get().subscribe(snapshot => {
        const task = new Task(snapshot);

        subscriber.next(task);
      });
    });
  }
}
