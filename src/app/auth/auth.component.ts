import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  email: string;
  password: string;
  
  constructor(
    public auth: AuthService
  ) { }

  ngOnInit(): void {
  }

  logIn() {
    this.auth.logIn(this.email, this.password);
    this.email = '';
    this.password = '';
  }

  logOut() {
    this.auth.logOut();
  }
}
