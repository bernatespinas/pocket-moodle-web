import { Component, OnInit } from '@angular/core';
import { Task } from '../topic';
import { TaskService } from '../task.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-task-detail',
  templateUrl: './task-detail.component.html',
  styleUrls: ['./task-detail.component.css']
})
export class TaskDetailComponent implements OnInit {
  task: Task;

  constructor(
    private taskService: TaskService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const subjectId = routeParams.get('subjectId');
    const topicId = routeParams.get('topicId');
    const taskItemId = routeParams.get('taskItemId');
    const taskItemNode: string = this.route.snapshot.url[0].path;

    if (taskItemNode === 'task') {
      this.taskService.getTask(subjectId, topicId, taskItemId)
        .subscribe(task => this.task = task)
      ;
    } else if (taskItemNode === 'exam') {
      this.taskService.getExam(subjectId, topicId, taskItemId)
        .subscribe(exam => this.task = exam)
      ;
    }
  }

}
