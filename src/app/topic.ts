import { DocumentData, QueryDocumentSnapshot, QuerySnapshot, DocumentSnapshot } from '@angular/fire/firestore';

export class Topic {
    id: string;

    name: string;
    visible: boolean;

    constructor(snapshot: QueryDocumentSnapshot<DocumentData>) {
        this.id = snapshot.id;

        this.name = snapshot.data().name;
        this.visible = snapshot.data().visible;
    }
}

export class Task {
    id: string;

    name: string;
    description: string;

    constructor(snapshot: QueryDocumentSnapshot<DocumentData>) {
        this.id = snapshot.id;

        this.name = snapshot.data().name;
        this.description = snapshot.data().description;
    }
}

export class Note {
    id: string;

    name: string;

    constructor(snapshot: QueryDocumentSnapshot<DocumentData>) {
        this.id = snapshot.id;

        this.name = snapshot.data().name;
    }
}