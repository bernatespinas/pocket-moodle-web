import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TopicService } from '../topic.service';
import { Topic } from '../topic';
import { SubjectService } from '../subject.service';

@Component({
  selector: 'app-subject-detail',
  templateUrl: './subject-detail.component.html',
  styleUrls: ['./subject-detail.component.css']
})
export class SubjectDetailComponent implements OnInit {
  subjectId: string;
  subjectName: string;

  topics: Map<string, Topic>;

  constructor(
    private topicService: TopicService,
    private subjectService: SubjectService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.fetchSubject();
  }

  private fetchSubject(): void  {
    this.subjectId = this.route.snapshot.paramMap.get('id');

    this.subjectService.getSubjectById(this.subjectId)
      .subscribe(subject => this.subjectName = subject.name)
    ;

    this.topicService.getTopics(this.subjectId)
      .subscribe(topics => this.topics = topics)
    ;
  }
}
