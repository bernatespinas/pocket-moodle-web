import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TimetableComponent } from './timetable/timetable.component';
import { SubjectsComponent } from './subjects/subjects.component';
import { MessagesComponent } from './messages/messages.component';
import { SubjectDetailComponent } from './subject-detail/subject-detail.component';
import { TopicDetailComponent } from './topic-detail/topic-detail.component';
import { TaskDetailComponent } from './task-detail/task-detail.component';

const routes: Routes = [
  { path: 'dashboard', component: DashboardComponent },

  { path: 'subjects', component: SubjectsComponent },
  { path: 'subject/:id', component: SubjectDetailComponent },
  { path: 'subject/:subjectId/:topicId', component: TopicDetailComponent },

  { path: 'task/:subjectId/:topicId/:taskItemId', component: TaskDetailComponent },
  { path: 'exam/:subjectId/:topicId/:taskItemId', component: TaskDetailComponent },

  { path: 'timetable', component: TimetableComponent },
  { path: 'messages', component: MessagesComponent },

  { path: '', redirectTo: '/dashboard', pathMatch: 'full' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
