import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TopicService } from '../topic.service';
import { Task, Note } from '../topic';

@Component({
  selector: 'app-topic-detail',
  templateUrl: './topic-detail.component.html',
  styleUrls: ['./topic-detail.component.css']
})
export class TopicDetailComponent implements OnInit {
  subjectId: string;
  topicId: string;

  tasks: Map<string, Task>;
  exams: Map<string, Task>;
  notes: Map<string, Note>;

  constructor(
    private topicService: TopicService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.subjectId = this.route.snapshot.paramMap.get('subjectId');
    this.topicId = this.route.snapshot.paramMap.get('topicId');

    this.topicService.getTasks(this.subjectId, this.topicId).subscribe(
      tasks => this.tasks = tasks
    );

    this.topicService.getExams(this.subjectId, this.topicId).subscribe(
      exams => this.exams = exams
    );

    this.topicService.getNotes(this.subjectId, this.topicId).subscribe(
      notes => this.notes = notes
    );
  }

}
