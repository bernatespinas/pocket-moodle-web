import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Task, Note, Topic } from './topic';

@Injectable({
  providedIn: 'root'
})
export class TopicService {

  constructor(
    private firestore: AngularFirestore
  ) { }

  getTopics(subjectId: string): Observable<Map<string, Topic>> {
    return new Observable(subscriber => {
      this.firestore
        .collection('subjects').doc(subjectId)
        .collection('topics')
      .get().subscribe(snapshot => {
        let topics: Map<string, Topic> = new Map();

        snapshot.forEach(topicSnapshot => {
          let topic = new Topic(topicSnapshot);

          topics.set(
            topic.id,
            topic
          );
        });

        subscriber.next(topics);
      });
    });
  }

  getTasks(subjectId: string, topicId: string): Observable<Map<string, Task>> {
    return this.getTaskItems(subjectId, topicId, 'tasks');
  }

  getExams(subjectId: string, topicId: string): Observable<Map<string, Task>> {
    return this.getTaskItems(subjectId, topicId, 'exams');
  }

  private getTaskItems(
    subjectId: string,
    topicId: string,
    taskItemNode: string
  ): Observable<Map<string, Task>> {
    return new Observable(subscriber => {
      this.firestore
        .collection('subjects').doc(subjectId)
        .collection('topics').doc(topicId)
        .collection(taskItemNode)
      .get().subscribe(snapshot => {
        let taskItems: Map<string, Task> = new Map();

        snapshot.forEach(taskItemSnapshot => {
          const task = new Task(taskItemSnapshot);

          taskItems.set(
            task.id,
            task
          );
        });

        subscriber.next(taskItems);
      });
    });
  }

  getNotes(subjectId: string, topicId: string): Observable<Map<string, Note>> {
    return new Observable(subscriber => {
      this.firestore
        .collection('subjects').doc(subjectId)
        .collection('topics').doc(topicId)
        .collection('notes')
      .get().subscribe(snapshot => {
        let notes: Map<string, Note> = new Map();

        snapshot.forEach(noteSnapshot => {
          const note = new Note(noteSnapshot);

          notes.set(
            note.id,
            note
          );
        });

        subscriber.next(notes);
      });
    });
  }
}
