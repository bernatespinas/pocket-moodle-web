import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentData, QueryDocumentSnapshot } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TimetableService {

  constructor(
    private firestore: AngularFirestore
  ) { }

  getHours(): Observable<string[]> {
    return new Observable((subscriber) => {
      this.firestore
        .collection('timetable').doc('hours')
      .get().subscribe(snapshot => {
        let hours: string[] = [];

        for (const hourEntry of Object.entries(snapshot.data())) {
          hours.push(hourEntry[1]);
        }

        subscriber.next(hours);
      });
    });
  }

  /// Each TimetableSubject[] corresponds to an hour range (a <tr>).
  getSubjects(): Observable<Map<number, TimetableSubject[]>> {
    return new Observable((subscriber) => {
      this.firestore.collection('timetable').doc('days').get()
        .subscribe(snapshot => {
          const weekdays = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday'];

          let timetableSubjects: Map<number, TimetableSubject[]> = new Map();

          for (const weekday of weekdays) {
            console.log("weekday: ", weekday);
            const daySubjects = this.getDaySubjects(snapshot, weekday);
            console.log("daySubjects in getSubjects: ", daySubjects);
            
            for (const daySubject of daySubjects.entries()) {
              console.log(`timetableSubjects.get(${daySubject[0]}): ${timetableSubjects.get(daySubject[0])}`);

              if (!timetableSubjects.has(daySubject[0])) {
                timetableSubjects.set(
                  daySubject[0],
                  [daySubject[1]]
                );
              } else {
                timetableSubjects.get(daySubject[0]).push(daySubject[1]);
              }
            }
          }

          console.log("timetableSubjects:", timetableSubjects);
          subscriber.next(timetableSubjects);
        })
      ;
    });
  }

  private getDaySubjects(snapshot: QueryDocumentSnapshot<DocumentData>, weekday: string): Map<number, TimetableSubject> {
    let daySubjects: Map<number, TimetableSubject> = new Map();

    console.log("searching for weekday: ", weekday);
    console.log("effing snapshot is: ", snapshot.data());
    console.log("child exists: ", snapshot.data().hasOwnProperty(weekday));

    let previousSubject: TimetableSubject = null;
    // Start at -1 so after the first iteration the first subject would be at 0.
    let hourRange = -1;

    for (const [key, value] of Object.entries(snapshot.data()[weekday])) {
      console.log("weekday: ", weekday, " subject: ", key);
      let subject: string = value.toString();
      console.log("subject: ", subject);

      if (previousSubject !== null) {
        if (previousSubject.subject === subject) {
          // If the previous subject is the same as the current one,
          // increate its length (it lasts at least one hour more).
          previousSubject.length += 1;
        } else {
          // If the previous subject is diferent, store it and
          // keep an eye on the current one.
          daySubjects.set(
            hourRange - previousSubject.length+1,
            previousSubject
          );

          previousSubject = new TimetableSubject(subject, 1);
        }
      } else {
        // If there was no previous subject (first iteration? TODO gaps)...
        previousSubject = new TimetableSubject(subject, 1);
      }

      hourRange += 1;
    }

    // Display the last found subject.
    if (previousSubject !== null) {
      daySubjects.set(
        hourRange - previousSubject.length+1,
        previousSubject
      );
    }

    console.log("u better not be empty: ", daySubjects);

    return daySubjects;
  }
}

export class TimetableSubject {
  subject: string;
  length: number;

  constructor(subject: string, length: number) {
    this.subject = subject;
    this.length = length;
  }
}
