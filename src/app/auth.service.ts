import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { User } from 'firebase';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user$: Observable<User>;
  done$: Observable<boolean>;

  email$: Observable<string>;
  fullName$: Observable<string>;
  pictureUrl$: Observable<string>;
  
  constructor(
    private auth: AngularFireAuth,
    private firestore: AngularFirestore,
    private router: Router
  ) {
    this.done$ = auth.authState.pipe(
      switchMap(user => {
        if (user) {
          this.updateUserData(user);
          this.user$ = of(user);
          // return of(true);
        } else {
          this.user$ = of(null);
          // return of(true);
        }

        return of(true);
      })
    );
  }

  async logIn(email: string, password: string) {
    this.auth.signInWithEmailAndPassword(email, password).then(
      cred => {
        this.updateUserData(cred.user);
      }
    ).catch(err => {
      console.log('catch:', err.message);
    });
  }

  private updateUserData(authUser: User): void {
    this.firestore
      .collection('users').doc(authUser.uid)
    .get().subscribe(snapshot => {
      this.fullName$ = of(snapshot.data().fullName);
      this.pictureUrl$ = of(snapshot.data().pictureUrl);
      this.email$ = of(authUser.email);

      this.user$ = of(authUser);
    });
  }
  
  logOut(): void {
    this.auth.signOut().then(() => {
      this.router.navigate(['/']);
      this.user$ = of(null);
    });
  }
}
