import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { TimetableService, TimetableSubject } from '../timetable.service';
import { SubjectService } from '../subject.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-timetable',
  templateUrl: './timetable.component.html',
  styleUrls: ['./timetable.component.css']
})
export class TimetableComponent implements OnInit {
  weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];

  private getHours: Subscription;
  private getTimetableSubjects: Subscription;
  private getSubjects: Subscription;

  constructor(
    private timetableService: TimetableService,
    private subjectService: SubjectService
  ) { }

  ngOnInit(): void {
    this.getHours = this.timetableService.getHours().subscribe(hours => {

      this.getTimetableSubjects = this.timetableService.getSubjects().subscribe(timetableSubjects => {

        this.getSubjects = this.subjectService.getSubjects().subscribe(subjects => {

          let table = document.getElementById('timetable');
          
          for (let i = 0; i < hours.length; i += 1) {
            let tr = document.createElement('tr');

            let tdHourRange = document.createElement('td');
            tdHourRange.className = 'hour-range';
            tdHourRange.innerText = hours[i];

            tr.appendChild(tdHourRange);

            for (const timetableSubject of timetableSubjects.get(i)) {
              let tdSubject = document.createElement('td');
              tdSubject.innerText = timetableSubject.subject.toUpperCase();
              tdSubject.title = subjects.get(timetableSubject.subject)?.name || tdSubject.innerText;
              tdSubject.style.backgroundColor = subjects.get(timetableSubject.subject)?.color;
              tdSubject.rowSpan = timetableSubject.length;
              
              tr.appendChild(tdSubject);
            }

            table.appendChild(tr);
          }
        });
      });
    });
  }

  ngOnDestroy() {
    console.log("Destroying TimetableComponent and unsubscribing ¿from? Observables");
    this.getHours?.unsubscribe();
    this.getTimetableSubjects?.unsubscribe();
    this.getSubjects?.unsubscribe();
  }
}
